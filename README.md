# DisplacedDiphotonLimits

This repository contains several notebooks which can extract the calculated limits and plot the brazil limits, and interpretation plots. The notebooks run with the conda virtual environment named `analysis`. 

## Getting started

1. Installing the conda environment

The notebooks require a conda virtual environment to find all the required libraries to extract and plot data. The conda virtual packages are stored in the `environment_packages.yml` file. To create the virtual environment execute the following steps

```
conda env create  --name analysis --file=environment_packages.yml
```

Creating the conda virtual environment will take some time. Once the environment is created, one can activate the enviroment with the following command

```
conda activate analysis
```
Activate the conda enviroment and then create a Jupyter notebook kernel with this environment. To create a notebook kernel, type the following command

```
# First activate the enviroment
conda activate analysis
python -m ipykernel install --user --name=analysis
```

2. Creating a jupyter notebook

Once the installation of the conda environment and the jupyter notebook are successful, the user can create a jupyter notebook session. To start a jupyter session, type

`jupyter notebook`

The juypter notebook will be created in a web browser window. On the jupyter notebook session you will be able to run the `limit_extractor.ipynb` and the `limit_plotter.ipynb`


# Useful tips:

## Installing anaconda:

It is recommended that the user use anaconda to create virtual environment to run the GUI. The source code for anaconda can be found here: 
[anaconda](https://www.anaconda.com/download/)

## Git repository download:

If the git repository is too large to download, the argument `--depth=1` can be added to reduce the size of the download.

`git clone --depth=1 ssh://git@gitlab.cern.ch:7999/dpanchal/DisplacedDiphotonLimits.git`