import matplotlib
import matplotlib.pyplot as plt
import numpy as np

_USETEX = True
_LABEL_FONTSIZE = 22

def setupMatplotlib(*args, **kwargs):
    """Set up rc Params for matplotlib for ATLAS-style formatting"""
    import matplotlib
    
    matplotlib.rcParams['font.size'] = 16
    matplotlib.rcParams['text.usetex'] = False
    matplotlib.rcParams["xtick.major.size"] = 12
    matplotlib.rcParams["xtick.minor.size"] = 8
    matplotlib.rcParams["ytick.major.size"] = 12
    matplotlib.rcParams["ytick.minor.size"] = 8
    matplotlib.rcParams['xtick.major.pad']  = 8
    matplotlib.rcParams['ytick.major.pad']  = 8
    
    if len(kwargs.keys())>0:
        for rc_key, rc_value in kwargs.items():
            matplotlib.rcParams[rc_key] = rc_value

def setupMatplotlib(*args, **kwargs):
    """Set up rc Params for matplotlib for ATLAS-style formatting"""
    import matplotlib
    
    matplotlib.rcParams['font.size'] = 16
    matplotlib.rcParams['text.usetex'] = False
    matplotlib.rcParams["xtick.major.size"] = 12
    matplotlib.rcParams["xtick.minor.size"] = 8
    matplotlib.rcParams["ytick.major.size"] = 12
    matplotlib.rcParams["ytick.minor.size"] = 8
    matplotlib.rcParams['xtick.major.pad']  = 8
    matplotlib.rcParams['ytick.major.pad']  = 8
    
    if len(kwargs.keys())>0:
        for rc_key, rc_value in kwargs.items():
            matplotlib.rcParams[rc_key] = rc_value

def useTex(useTex=False):
    """Set LaTeX rendering for axis labels. Breaks for VS code"""
    _USETEX = useTex

def set_label_size(size=18):
    _LABEL_FONTSIZE = size

def stamp_sample_legend(ax, x_loc=0.70, y_loc=0.68, final_state="HyyHSM", add_text="", fontsize=18):
    """Stamp the signal sample details"""
    
    if final_state == "HyyHSM":
        br_text = r"$\mathcal{B}(\tilde{\chi}^{0}_{1} \rightarrow H + \tilde{G}) = $1"
        fs_text = r"$H \rightarrow \gamma\gamma$ final state"
        
    elif final_state == "ZeeZSM":
        br_text = r"$\mathcal{B}(\tilde{\chi}^{0}_{1} \rightarrow Z + \tilde{G}) = $1"
        fs_text = r"$Z \rightarrow e^{+}e^{-}$ final state"
    else:
        br_text = " "
        fs_text = " "
    
    ax.text(x_loc, y_loc,
            br_text,
            usetex=_USETEX,
            transform=ax.transAxes,
            fontsize=fontsize)
    
    if add_text != "":
        ax.text(x_loc, y_loc-0.05,
                add_text,
                usetex=_USETEX,
                transform=ax.transAxes,
                fontsize=fontsize)
        
        ax.text(x_loc, y_loc-0.10,
                fs_text,
                usetex=_USETEX,
                transform=ax.transAxes,
                fontsize=fontsize)
    else:
        ax.text(x_loc, y_loc-0.05,
                fs_text,
                usetex=_USETEX,
                transform=ax.transAxes,
                fontsize=fontsize)
        
    return ax

def plot_brazil(ax,
                x_grid, 
                theory_limit, 
                obs_limit, 
                exp_limit, 
                exp_limit_plus1, 
                exp_limit_minus1,
                exp_limit_plus2,
                exp_limit_minus2,
                mass_grid=True,
                labelsize=18):
    """Make the brazil plot"""
    
    assert len(x_grid) == len(theory_limit), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit), "Please provide equal size arrays"
    assert len(x_grid) == len(obs_limit), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_plus1), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_plus2), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_minus1), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_minus2), "Please provide equal size arrays"
        
    exp_line    = ax.plot(x_grid, exp_limit, "k--")
    obs_line    = ax.plot(x_grid, obs_limit, "ko-")
    theory_line = ax.plot(x_grid, theory_limit, "r-.")
    
    sigma2_band = ax.fill_between(x_grid,
                                  y1=exp_limit_minus2,
                                  y2=exp_limit_plus2,
                                  facecolor="yellow",
                                  alpha=1.0)
    
    sigma1_band = ax.fill_between(x_grid,
                                  y1=exp_limit_minus1,
                                  y2=exp_limit_plus1,
                                  facecolor="lawngreen",
                                  alpha=1.0)
    
    obs,        = plt.plot([],[], marker='o', color='black', linestyle='solid')
    exp,        = plt.plot([],[], color='black', linestyle='dashed')
    exp_fill,   = plt.fill(np.NaN, np.NaN, 'lawngreen', alpha=1.0)
    exp_fill_2, = plt.fill(np.NaN, np.NaN, 'yellow', alpha=1.0)
    theory,     = plt.plot([],[], color='red', linestyle='dashdot')

    ax.legend(handles=[obs, exp, exp_fill, exp_fill_2, theory], 
               labels=["Observed", "Expected", r"Expected $\pm$ 1$\sigma$", r"Expected $\pm$ 2$\sigma$", "Theory"], 
               fontsize=_LABEL_FONTSIZE,
               loc='upper right')
    
    x_label = r"m(${\tilde{\chi}^{0}_{1}}$) [GeV]" if mass_grid else r"$\tau({\tilde{\chi}^{0}_{1}}$) [ns]"
    ax.set_xlabel(x_label,
              usetex=_USETEX,
              fontsize=labelsize)
    
    ax.set_ylabel(r"95$\%$ C.L. limit on $\sigma$(pp$\rightarrow \tilde{\chi}^{0}_{1}\tilde{\chi}^{0}_{1})$ [fb]",
              usetex=_USETEX,
              fontsize=labelsize)

    
    return ax


def plot_exclusion_brazil(ax,
                          obs_limit, 
                          exp_limit, 
                          exp_limit_plus1, 
                          exp_limit_minus1,
                          exp_limit_plus2,
                          exp_limit_minus2,
                          y_grid,
                          x_label=r"m(${\tilde{\chi}^{0}_{1}}$) [GeV]",
                          y_label=r"$\tau({\tilde{\chi}^{0}_{1}}$) [ns]"):
    """Make the exclusion limit brazil plot"""
    
    assert len(y_grid) == len(exp_limit), "Please provide equal size arrays"
    assert len(y_grid) == len(obs_limit), "Please provide equal size arrays"
    assert len(y_grid) == len(exp_limit_plus1), "Please provide equal size arrays"
    assert len(y_grid) == len(exp_limit_plus2), "Please provide equal size arrays"
    assert len(y_grid) == len(exp_limit_minus1), "Please provide equal size arrays"
    assert len(y_grid) == len(exp_limit_minus2), "Please provide equal size arrays"
        
    exp_line    = ax.plot(exp_limit, y_grid, "k--")
    obs_line    = ax.plot(obs_limit, y_grid, "ko-")
    
    sigma2_band = ax.fill_betweenx(y_grid,
                                   x1=exp_limit_minus2,
                                   x2=exp_limit_plus2,
                                   facecolor="yellow",
                                   alpha=1.0)
    
    sigma1_band = ax.fill_betweenx(y_grid,
                                   x1=exp_limit_minus1,
                                   x2=exp_limit_plus1,
                                   facecolor="lawngreen",
                                   alpha=1.0)
    
    obs,        = plt.plot([],[], marker='o', color='black', linestyle='solid')
    exp,        = plt.plot([],[], color='black', linestyle='dashed')
    exp_fill,   = plt.fill(np.NaN, np.NaN, 'lawngreen', alpha=1.0)
    exp_fill_2, = plt.fill(np.NaN, np.NaN, 'yellow', alpha=1.0)

    ax.legend(handles=[obs, exp, exp_fill, exp_fill_2],
               labels=["Observed", "Expected", r"Expected $\pm$ 1$\sigma$", r"Expected $\pm$ 2$\sigma$"],
               fontsize=16,
               loc='upper right')
    
    ax.set_xlabel(x_label,
              usetex=_USETEX,
              fontsize=18)
    
    ax.set_ylabel(y_label,
              usetex=_USETEX,
              fontsize=18)

    
    return ax

def plot_one_sigma_brazil(ax,
                        x_grid, 
                        obs_limit, 
                        exp_limit, 
                        exp_limit_plus1, 
                        exp_limit_minus1,
                        x_label="",
                        y_label="",
                        label="",
                        labelsize=18,
                        color=None):
    """Make the brazil plot"""
    
    assert len(x_grid) == len(exp_limit), "Please provide equal size arrays"
    assert len(x_grid) == len(obs_limit), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_plus1), "Please provide equal size arrays"
    assert len(x_grid) == len(exp_limit_minus1), "Please provide equal size arrays"
        
    if color is None:
        one_sigma_color = "lawngreen"
        obs_color = "black"
    else:
        one_sigma_color = color
        obs_color = color

    exp_line    = ax.plot(x_grid, exp_limit, linestyle="dashed", color=obs_color)
    obs_line    = ax.plot(x_grid, obs_limit, linestyle="solid", color=obs_color, label=label)
        
    sigma1_band = ax.fill_between(x_grid,
                                  y1=exp_limit_minus1,
                                  y2=exp_limit_plus1,
                                  facecolor=one_sigma_color,
                                  alpha=0.5)
    ax.set_xlabel(x_label,
              usetex=_USETEX,
              fontsize=labelsize)
    
    ax.set_ylabel(y_label,
              usetex=_USETEX,
              fontsize=labelsize)

    
    return ax