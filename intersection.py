import numpy as np
from scipy.interpolate import make_interp_spline, BSpline, interp1d

def interpolated_intercepts(x, y1, y2):
    """Find the intercepts of two curves, given by the same x data"""

    def intercept(point1, point2, point3, point4):
        """find the intersection between two lines
        the first line is defined by the line between point1 and point2
        the first line is defined by the line between point3 and point4
        each point is an (x,y) tuple.

        So, for example, you can find the intersection between
        intercept((0,0), (1,1), (0,1), (1,0)) = (0.5, 0.5)

        Returns: the intercept, in (x,y) format
        """    

        def line(p1, p2):
            A = (p1[1] - p2[1])
            B = (p2[0] - p1[0])
            C = (p1[0]*p2[1] - p2[0]*p1[1])
            return A, B, -C

        def intersection(L1, L2):
            D  = L1[0] * L2[1] - L1[1] * L2[0]
            Dx = L1[2] * L2[1] - L1[1] * L2[2]
            Dy = L1[0] * L2[2] - L1[2] * L2[0]

            x = Dx / D
            y = Dy / D
            return x,y

        L1 = line([point1[0],point1[1]], [point2[0],point2[1]])
        L2 = line([point3[0],point3[1]], [point4[0],point4[1]])

        R = intersection(L1, L2)

        return R

    idxs = np.argwhere(np.diff(np.sign(y1 - y2)) != 0)

    xcs = []
    ycs = []

    for idx in idxs:
        xc, yc = intercept((x[idx], y1[idx]),((x[idx+1], y1[idx+1])), ((x[idx], y2[idx])), ((x[idx+1], y2[idx+1])))
        xcs.append(xc)
        ycs.append(yc)
    return np.array(xcs), np.array(ycs)

def perform_spline(x_points, y_points, resample=100, spline_type=3):
    """Perform spline of the coarse curve and smoothen the curve"""

    x_grid_fine = np.linspace( min(x_points), max(x_points), resample )
    
    if not isinstance(y_points, np.ndarray):
        y_points = np.array(y_points)
        
    if not isinstance(x_points, np.ndarray):
        x_points = np.array(x_points)
    
    y_points_finite = y_points[~np.isnan(y_points)]
    x_points_finite = x_points[~np.isnan(y_points)]
    
    if len(y_points_finite) <= 2:
        print("Cannot spline with only two or fewer points")
        return y_points, x_points, None
    
    # First try BSpline
    try:
        spline_curve = make_interp_spline(x_points_finite, y_points_finite, k=spline_type)
        
    # Otherwise try a linear interpolation
    except ValueError:
        spline_curve = interp1d(x_points_finite, y_points_finite, fill_value="extrapolate")
        
    smooth_curve = spline_curve(x_grid_fine)
    
    return smooth_curve, x_grid_fine, spline_curve

def get_smooth_band(exp_points, exp_plus1_points, exp_minus1_points):
    """Interpolate the 1-sigma band"""
    
    exp_minus1 = []
    exp_plus1  = [] 

    for ex, exp1, exm1 in zip(exp_points, exp_plus1_points, exp_minus1_points):
        
        exp_value    = ex[0] if len(ex) > 0 else np.NaN
        minus1_value = exm1[0] if len(exm1) > 0 else np.NaN
        plus1_value  = exp1[0] if len(exp1) > 0 else np.NaN
            
#         print(f"exp = {exp_value}, minus1 = {minus1_value}, plus1 = {plus1_value}")
        
        # If all values are NaN the exclusion point is kaput
        if exp_value is np.NaN and minus1_value is np.NaN and plus1_value is np.NaN:
            exp_minus1.append(np.NaN)
            exp_plus1.append(np.NaN)

        # If both the 1-sigma points exist then use them
        elif plus1_value is not np.NaN and minus1_value is not np.NaN:
            if minus1_value > plus1_value:
                exp_minus1.append(plus1_value)
                exp_plus1.append(minus1_value)
            else:
                exp_minus1.append(minus1_value)
                exp_plus1.append(plus1_value)
            
        # If the plus1 value doesn't exist then use the minus1 value symmetrized
        elif plus1_value is np.NaN and exp_value is not np.NaN and minus1_value is not np.NaN:
            # If the minus1 value is on the right of expected, flip the 1-sigma values
            if minus1_value > exp_value:
                exp_minus1.append( (exp_value - np.abs(minus1_value - exp_value)) )
                exp_plus1.append(minus1_value)
            else:
                exp_minus1.append(minus1_value)
                exp_plus1.append( (exp_value + np.abs(minus1_value - exp_value)) )
                
        # If the minus1 value doesn't exist then use the plus1 value symmetrized
        elif minus1_value is np.NaN and exp_value is not np.NaN and plus1_value is not np.NaN:
            # If the minus1 value is on the right of expected, flip the 1-sigma values
            if plus1_value < exp_value:
                exp_minus1.append(plus1_value)
                exp_plus1.append( (exp_value + np.abs(plus1_value - exp_value)) )
            else:
                exp_minus1.append( (exp_value - np.abs(plus1_value - exp_value)) )
                exp_plus1.append(plus1_value)
        
        # If none of the 1-sigma values exist, the exp value is kaput
        else:
            exp_minus1.append(np.NaN)
            exp_plus1.append(np.NaN)
            
    
    return exp_minus1, exp_plus1

def intersection_wrapper(contour_grid,
                         x_grid,
                         theory_curve,
                         obs_curve,
                         exp_curve,
                         exp_plus1_curve,
                         exp_minus1_curve,
                         exp_plus2_curve,
                         exp_minus2_curve):
    
    
    xc_plus1  = []
    yc_plus1  = []

    xc_plus2  = []
    yc_plus2  = []

    xc_minus1 = []
    yc_minus1 = []

    xc_minus2 = []
    yc_minus2 = []

    xc_obs    = []
    yc_obs    = []

    xc_exp    = []
    yc_exp    = []
    
    for i_contour in range(len(contour_grid)):

        xc_p1, yc_p1 = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               exp_plus1_curve[:,i_contour])

        xc_plus1.append(xc_p1[~np.isnan(xc_p1)].tolist())
        yc_plus1.append(yc_p1[~np.isnan(yc_p1)].tolist())

        xc_p2, yc_p2 = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               exp_plus2_curve[:,i_contour])

        xc_plus2.append(xc_p2[~np.isnan(xc_p2)].tolist())
        yc_plus2.append(yc_p2[~np.isnan(yc_p2)].tolist())


        xc_m1, yc_m1 = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               exp_minus1_curve[:,i_contour])

        xc_minus1.append(xc_m1[~np.isnan(xc_m1)].tolist())
        yc_minus1.append(yc_m1[~np.isnan(yc_m1)].tolist())


        xc_m2, yc_m2 = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               exp_minus2_curve[:,i_contour])

        xc_minus2.append(xc_m2[~np.isnan(xc_m2)].tolist())
        yc_minus2.append(yc_m2[~np.isnan(yc_m2)].tolist())    

        xc_ex, yc_ex = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               exp_curve[:,i_contour])

        xc_exp.append(xc_ex[~np.isnan(xc_ex)].tolist())
        yc_exp.append(yc_ex[~np.isnan(yc_ex)].tolist())

        xc_ob, yc_ob = interpolated_intercepts(x_grid,
                                               theory_curve[:,i_contour],
                                               obs_curve[:,i_contour])
        
        xc_obs.append(xc_ob[~np.isnan(xc_ob)].tolist())
        yc_obs.append(yc_ob[~np.isnan(yc_ob)].tolist())
        
    return {
        "obs"    : (xc_obs, yc_obs),
        "exp"    : (xc_exp, yc_exp),
        "plus1"  : (xc_plus1, yc_plus1),
        "plus2"  : (xc_plus2, yc_plus2),
        "minus1" : (xc_minus1, yc_minus1),
        "minus2" : (xc_minus2, yc_minus2)
    }

def spline_wrapper( 
                    contour_grid,
                    variable_grid,
                    obs_upperlimit,
                    exp_upperlimit,
                    exp_upperlimit_plus1,
                    exp_upperlimit_minus1,
                    exp_upperlimit_plus2,
                    exp_upperlimit_minus2,
                    extrapolation_range=None,
                    ):
    """Wrapper function to spline and smooth the brazil band and the theory curves"""

    from getXsection import XSECTION

    theory_curve_extrapolated     = []
    exp_curve_extrapolated        = []
    obs_curve_extrapolated        = []
    exp_plus1_curve_extrapolated  = []
    exp_plus2_curve_extrapolated  = []
    exp_minus1_curve_extrapolated = []
    exp_minus2_curve_extrapolated = []

    if not isinstance(variable_grid, np.ndarray):
        variable_grid = np.array(variable_grid)

    if not isinstance(contour_grid, np.ndarray):
        contour_grid = np.array(contour_grid)

    # Create a finely sampled x-grid
    if extrapolation_range is None:
        low_xval  = variable_grid[0]
        high_xval = variable_grid[-1]
    elif len(extrapolation_range) != 2:
        print("Please provide only two values, a low- and a high-value")
        low_xval  = variable_grid[0]
        high_xval = variable_grid[-1]
    else:
        low_xval  = extrapolation_range[0]
        high_xval = extrapolation_range[1]

    step_size = 1
    num_steps = int(high_xval - low_xval / step_size) + 1
    variable_grid_fine = np.linspace(low_xval, high_xval, num_steps)

    # Create a finely sampled cross section curve from the SUSY cross section values
    cross_sections = [_ for _ in XSECTION.values()]
    theory_curve_susy = []

    for idx in range(len(contour_grid)):
        theory_curve_susy.append(cross_sections)

    # Convert a list of list into a numpy array and transpose it to match the dimensions of the brazil band arrays
    theory_curve_susy = np.array(theory_curve_susy)
    theory_curve_susy = theory_curve_susy.T

    spline_type = 2

    for i_contour in range(len(contour_grid)):

        theory_smooth, _, theory_spline_curve = perform_spline(list(XSECTION.keys()), theory_curve_susy[:,i_contour], spline_type=spline_type)
        if theory_spline_curve is not None:        
            theory_curve_extrapolated.append(theory_spline_curve(variable_grid_fine))
        else: 
            theory_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))

        obs_smooth, _, obs_spline_curve = perform_spline(variable_grid, obs_upperlimit[:,i_contour], spline_type=spline_type)
        if obs_spline_curve is not None:
            obs_curve_extrapolated.append(obs_spline_curve(variable_grid_fine))
        else: 
            obs_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))

        exp_smooth, _, exp_spline_curve = perform_spline(variable_grid, exp_upperlimit[:,i_contour], spline_type=spline_type)
        if exp_spline_curve is not None:
            exp_curve_extrapolated.append(exp_spline_curve(variable_grid_fine))
        else:
            exp_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))
        
        exp_plus1_smooth, _, exp_spline_plus1_curve = perform_spline(variable_grid, exp_upperlimit_plus1[:,i_contour], spline_type=spline_type)
        if exp_spline_plus1_curve is not None:
            exp_plus1_curve_extrapolated.append(exp_spline_plus1_curve(variable_grid_fine))
        else: 
            exp_plus1_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))

        exp_plus2_smooth, _, exp_spline_plus2_curve = perform_spline(variable_grid, exp_upperlimit_plus2[:,i_contour], spline_type=spline_type)
        if exp_spline_plus2_curve is not None:
            exp_plus2_curve_extrapolated.append(exp_spline_plus2_curve(variable_grid_fine))
        else: 
            exp_plus2_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))

        exp_minus1_smooth, _, exp_spline_minus1_curve = perform_spline(variable_grid, exp_upperlimit_minus1[:,i_contour], spline_type=spline_type)
        if exp_spline_minus1_curve is not None:
            exp_minus1_curve_extrapolated.append(exp_spline_minus1_curve(variable_grid_fine))
        else:
            exp_minus1_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))
        
        exp_minus2_smooth, _, exp_spline_minus2_curve = perform_spline(variable_grid, exp_upperlimit_minus2[:,i_contour], spline_type=spline_type)
        if exp_spline_minus2_curve is not None:
            exp_minus2_curve_extrapolated.append(exp_spline_minus2_curve(variable_grid_fine))
        else:
            exp_minus2_curve_extrapolated.append(np.full(variable_grid_fine.shape, fill_value=np.NaN))
        
        
    # Convert to numpy array
    theory_curve_extrapolated     = np.array(theory_curve_extrapolated)
    exp_curve_extrapolated        = np.array(exp_curve_extrapolated)
    obs_curve_extrapolated        = np.array(obs_curve_extrapolated)
    exp_plus1_curve_extrapolated  = np.array(exp_plus1_curve_extrapolated)
    exp_plus2_curve_extrapolated  = np.array(exp_plus2_curve_extrapolated)
    exp_minus1_curve_extrapolated = np.array(exp_minus1_curve_extrapolated)
    exp_minus2_curve_extrapolated = np.array(exp_minus2_curve_extrapolated)   

    return {
        "theory_curve"     : theory_curve_extrapolated,
        "exp_curve"        : exp_curve_extrapolated,
        "obs_curve"        : obs_curve_extrapolated,
        "exp_plus1_curve"  : exp_plus1_curve_extrapolated,
        "exp_plus2_curve"  : exp_plus2_curve_extrapolated,
        "exp_minus1_curve" : exp_minus1_curve_extrapolated,
        "exp_minus2_curve" : exp_minus2_curve_extrapolated,
        "x_grid_fine"      : variable_grid_fine
    }